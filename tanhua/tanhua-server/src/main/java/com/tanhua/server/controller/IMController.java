package com.tanhua.server.controller;

import com.tanhua.server.service.IMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("messages")
public class IMController {

    @Autowired
    private IMService imService;

    /**
     * 接口名称：联系人添加
     * 接口路径：POST/messages/contacts
     */
    @PostMapping("contacts")
    public ResponseEntity<Object> addContacts(@RequestBody Map<String, Object> map) {
        Integer friendId = (Integer) map.get("userId");
        return imService.addContacts(friendId.longValue());
    }

    /**
     * 接口名称：联系人列表
     * 接口路径：GET/messages/contacts
     */
    @GetMapping("contacts")
    public ResponseEntity<Object> contactsList(
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer pagesize, String keyword) {
        return imService.contactsList(page, pagesize, keyword);
    }
}
