package com.tanhua.server.service;

import com.tanhua.commons.template.HuanXinTemplate;
import com.tanhua.domain.db.UserInfo;
import com.tanhua.domain.mongo.Friend;
import com.tanhua.domain.vo.ContractVo;
import com.tanhua.domain.vo.PageResult;
import com.tanhua.dubbo.api.UserInfoApi;
import com.tanhua.dubbo.api.mongo.FriendApi;
import com.tanhua.server.interceptor.UserHolder;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class IMService {
    @Reference
    private FriendApi friendApi;
    @Autowired
    private HuanXinTemplate huanXinTemplate;
    @Reference
    private UserInfoApi userInfoApi;
    /**
     * 接口名称：联系人添加
     */
    public ResponseEntity<Object> addContacts(Long friendId) {
        //1. 获取用户id
        Long userId = UserHolder.getUserId();
        //2. 保存好友关系到mongodb中的tanhua_users表中
        friendApi.save(userId,friendId);
        //3. 注册好友关系到环信
        huanXinTemplate.contactUsers(userId,friendId);
        return ResponseEntity.ok(null);
    }

    /**
     * 接口名称：联系人列表
     */
    public ResponseEntity<Object> contactsList(Integer page, Integer pagesize, String keyword) {

        Long userId = UserHolder.getUserId();
        //1. 分页查询联系人
        PageResult pageResult = friendApi.findFriendByUserId(userId,page,pagesize);
        //2. 获取分页查询的数据
        List<Friend> friendList = (List<Friend>) pageResult.getItems();

        //3. 创建并封装返回的vo集合
        List<ContractVo> voList = new ArrayList<>();
        // 遍历好友列表
        if (friendList != null && friendList.size()>0) {
            for (Friend friend : friendList) {
                // 获取好友id
                Long friendId = friend.getFriendId();
                // 根据好友id查询
                UserInfo userInfo = userInfoApi.findById(friendId);
                // 创建返回的vo对象
                ContractVo vo = new ContractVo();
                // 对象拷贝
                BeanUtils.copyProperties(userInfo,vo);
                // 设置好友用户id
                vo.setUserId(friendId.toString());
                // 添加到集合
                voList.add(vo);
            }
        }

        // 设置到pageResult中
        pageResult.setItems(voList);
        return ResponseEntity.ok(pageResult);
    }
}
