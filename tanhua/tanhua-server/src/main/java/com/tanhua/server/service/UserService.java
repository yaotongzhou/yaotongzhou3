package com.tanhua.server.service;

import com.alibaba.fastjson.JSON;
import com.tanhua.commons.template.AipFaceTemplate;
import com.tanhua.commons.template.HuanXinTemplate;
import com.tanhua.commons.template.OssTemplate;
import com.tanhua.commons.template.SmsTemplate;
import com.tanhua.domain.db.User;
import com.tanhua.domain.db.UserInfo;
import com.tanhua.domain.vo.ErrorResult;
import com.tanhua.domain.vo.PageResult;
import com.tanhua.domain.vo.UserInfoVo;
import com.tanhua.domain.vo.UserLikeVo;
import com.tanhua.dubbo.api.UserApi;
import com.tanhua.dubbo.api.UserInfoApi;
import com.tanhua.dubbo.api.mongo.FriendApi;
import com.tanhua.dubbo.api.mongo.UserLikeApi;
import com.tanhua.server.interceptor.UserHolder;
import com.tanhua.server.utils.JwtUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;

/**
 * 创建UserService，处理业务
 */
@Service
public class UserService {
    /**
     * @Reference
     * 1. 注入dubbo服务接口的代理对象，通过代理对象远程调用
     * 2. 注入导入的包:
     *   org.apache.dubbo.config.annotation.Reference  正确
     *   jdk.nashorn.internal.ir.annotations.Reference 错误
     */
    @Reference
    private UserApi userApi;
    @Reference
    private UserInfoApi userInfoApi;
    @Autowired
    private SmsTemplate smsTemplate;
    @Autowired
    private AipFaceTemplate aipFaceTemplate;
    @Autowired
    private OssTemplate ossTemplate;
    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    @Autowired
    private HuanXinTemplate huanXinTemplate;
    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    private final String SMS_KEY = "SMS_KEY_";
    private final String TOKEN_KEY = "TOKEN_KEY_";
    // 获取配置文件值
    @Value("${tanhua.secret}")
    private String secret;

    /**
     * 测试方法1：根据手机号码查询
     */
    public ResponseEntity<Object> findByMobile(String mobile) {
        User user = userApi.findByMobile(mobile);
        return ResponseEntity.ok(user);
    }
    /**
     * 测试方法2：保存
     */
    public ResponseEntity<Object> save(User user) {
        try {
            int i = 1/0;
            Long id = userApi.save(user);
            return ResponseEntity.ok(id);
        } catch (Exception e) {
            e.printStackTrace();
            // 返回异常
            Map<String,String> errorMap = new HashMap<>();
            errorMap.put("errCode","1200");
            errorMap.put("errMessage","保存异常，请练习管理员！");
            return ResponseEntity.status(500).body(errorMap);
        }
    }


    /**
     * 接口名称：登录第一步---手机号登录
     * 需求描述：输入手机号码，发送验证码，存储到redis中,设置有效时间5分钟
     */
    public ResponseEntity<Object> login(String phone) {
        //1. 生成六位验证码
        String code = (int)((Math.random() * 9 + 1) * 100000) + "";
        //2. 通过SmsTemplate，发送验证码
        //smsTemplate.sendSms(phone,code);
        code = "123456";
        //3. 验证码存储到redis中
        redisTemplate.opsForValue().set(SMS_KEY+phone,code, Duration.ofMinutes(10));
        return ResponseEntity.ok(null);
    }

    public static void main(String[] args) {
        String code = (int)((Math.random() * 9 + 1) * 100000) + "";
        System.out.println("code = " + code);
    }


    /**
     * 接口名称：登录第二步---验证码校验
     */
    public ResponseEntity<Object> loginVerification(String phone, String code) {
        //1. 从redis中获取验证码
        String key = SMS_KEY + phone;
        String redisCode = redisTemplate.opsForValue().get(key);
        //2. 验证码校验
        if (StringUtils.isEmpty(redisCode) || !code.equals(redisCode)) {
            // 返回异常
            return ResponseEntity.status(500).body(ErrorResult.error());
        }

        //3. 删除验证码
        redisTemplate.delete(key);

        //4. 根据手机号码查询，判断是否需要自动注册
        User user = userApi.findByMobile(phone);
        boolean isNew = false;
        // 【日志类型：默认是0101 表示登陆】
        String type = "0101";
        if (user == null) {
            // 手机号码不存在，自动注册
            user = new User();
            user.setMobile(phone);
            user.setPassword(DigestUtils.md5Hex("123456"));
            // 【保存用户，接收返回值】
            Long userId = userApi.save(user);
            user.setId(userId);
            // 新用户
            isNew = true;
            // 新用户注册到环信
            huanXinTemplate.register(user.getId());

            // 【注册】
            type = "0102";
        }

        // 【构造map，封装消息内容】
        Map<String,String> logMap = new HashMap<>();
        logMap.put("userId",user.getId().toString());
        logMap.put("type",type);
        logMap.put("date",new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        // 【map转换为json】
        String logString = JSON.toJSONString(logMap);
        try {
            //【发送一条MQ消息，消息内容：userId、type、date】
            rocketMQTemplate.convertAndSend("tanhua-log2",logString);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        //【需求：生成token并返回】
        String token = JwtUtils.createToken(user.getId().toString(), user.getMobile(), secret);
        // 存储用户数据  key:token  value:用户json数据
        // user对象转换为json
        String userJsonString = JSON.toJSONString(user);
        // 存储到redis中
        redisTemplate.opsForValue().set(TOKEN_KEY+token,userJsonString,Duration.ofHours(4));

        //5. 返回
        Map<String,Object> resultMap = new HashMap<>();
        resultMap.put("token",token);
        resultMap.put("isNew",isNew);
        return ResponseEntity.ok(resultMap);
    }

    /**
     * 接口名称：新用户---1填写资料
     * 需求分析：完善用户信息，保存到tb_user_info表中
     */
    public ResponseEntity<Object> saveUserInfo(UserInfo userInfo, String token) {
        //1. 根据token获取用户
        User user = findUserByToken(token);

        //2. 判断
        if (user == null) {
            return ResponseEntity.status(500).body(ErrorResult.error());
        }

        //3. 设置用户详情userInfo的id
        userInfo.setId(user.getId());
        //4. 保存
        userInfoApi.save(userInfo);
        return ResponseEntity.ok(null);
    }

    /**
     * 根据token，查找redis获取登陆用户对象json字符串
     * @param token
     * @return
     */
    public User findUserByToken(String token) {
        // 拼接redis中的key
        String key = TOKEN_KEY+token;
        // 根据key获取用户信息的json字符串
        String userJsonString = redisTemplate.opsForValue().get(key);
        // 判断
        if (StringUtils.isEmpty(userJsonString)) {
            return null;
        }
        // string--->user
        User user = JSON.parseObject(userJsonString, User.class);
        return user;
    }

    /**
     * 接口名称：新用户---2选取头像
     * 接口路径：POST/user/loginReginfo/head
     * 需求分析：修改tb_user_info表，设置头像
     */
    public ResponseEntity<Object> updateHead(MultipartFile headPhoto, String token) throws IOException {
        //1. 根据token获取用户
        User user = findUserByToken(token);
        //2. 判断
        if (user == null) {
            return ResponseEntity.status(500).body(ErrorResult.error());
        }

        //3. 先进行人脸检测
        boolean detect = aipFaceTemplate.detect(headPhoto.getBytes());
        if (!detect) {
            return ResponseEntity.status(500).body(ErrorResult.faceError());
        }

        //4. 头像上传到阿里云OSS存储，返回图片地址
        String url =
                ossTemplate.upload(headPhoto.getOriginalFilename(), headPhoto.getInputStream());

        //4. 修改UserInfo
        UserInfo userInfo = new UserInfo();
        userInfo.setId(user.getId());
        userInfo.setAvatar(url);
        userInfoApi.update(userInfo);
        return ResponseEntity.ok(null);
    }

    /**
     * 需求描述：根据token中的用户id，查询用户信息
     * @param userID
     * @param huanxinID
     */
    public ResponseEntity<Object> findById(Long userID, Long huanxinID) {
        //1. 根据token获取用户
        Long userId = UserHolder.getUserId();

        //2. 判断 用户id
        if (userID != null) {
            userId = userID;
        }
        if (huanxinID != null) {
            userId = huanxinID;
        }

        //3. 根据用户id查询
        UserInfo userInfo = userInfoApi.findById(userId);
        //4. 创建vo、封装vo
        UserInfoVo vo = new UserInfoVo();
        // 对象拷贝
        BeanUtils.copyProperties(userInfo,vo);
        // 处理年龄, 要返回字符串
        if (userInfo.getAge() != null) {
            vo.setAge(userInfo.getAge().toString());
        }
        return ResponseEntity.ok(vo);
    }

    /**
     * 需求描述：更新用户信息
     */
    public ResponseEntity<Object> updateUserInfo(UserInfo userInfo) {
        //1. 根据token获取用户
        User user = UserHolder.get();
        //2. 判断
        if (user == null) {
            return ResponseEntity.status(500).body(ErrorResult.error());
        }
        //3. 设置用户id
        userInfo.setId(user.getId());
        //4. 修改
        userInfoApi.update(userInfo);
        return ResponseEntity.ok(null);
    }

    @Reference
    private UserLikeApi userLikeApi;

    /**
     * 接口名称：互相喜欢，喜欢，粉丝
     * 接口路径：GET/users/counts
     */
    public ResponseEntity<Object> queryCounts() {
        //1. 获取登陆用户
        Long userId = UserHolder.getUserId();
        //2. 调用api服务查询
        //2.1 统计互相关注
        Long eachLoveCount = userLikeApi.queryEachLoveCount(userId);
        //2.2 喜欢
        Long loveCount = userLikeApi.queryLoveCount(userId);
        //2.3 粉丝
        Long fanCount = userLikeApi.queryFanCount(userId);

        //3. 返回数据
        Map<String,Integer> resultMap = new HashMap<>();
        resultMap.put("eachLoveCount",eachLoveCount.intValue());
        resultMap.put("loveCount",loveCount.intValue());
        resultMap.put("fanCount",fanCount.intValue());
        return ResponseEntity.ok(resultMap);
    }

    /**
     * 接口名称：互相喜欢、喜欢、粉丝、谁看过我
     * 路径参数type:
     *   1 互相关注: tanhua_users
     *   2 我关注: user_like
     *   3 粉丝: user_like
     *   4 谁看过我: visitors
     */
    public ResponseEntity<Object> queryUserLikeList(
            Integer page, Integer pagesize, Integer type) {
        // 1. 获取登陆用户id
        Long userId = UserHolder.getUserId();
        // 2. 调用Api服务查询，返回pageResult分页对象，内容放的数据 Map
        PageResult pageResult = null;
        switch (type){
            case 1: // 互相关注: tanhua_users
                pageResult = userLikeApi.queryEachLoveList(userId,page,pagesize);
                break;
            case 2: // 我关注: user_like
                pageResult = userLikeApi.queryUserLikeList(userId,page,pagesize);
                break;
            case 3: // 粉丝: user_like
                pageResult = userLikeApi.queryFansList(userId,page,pagesize);
                break;
            case 4: // 谁看过我: visitors
                pageResult = userLikeApi.queryVisitorsList(userId,page,pagesize);
                break;
        }
        //3. 获取查询的数据
        List<Map<String,Object>> list = (List<Map<String, Object>>) pageResult.getItems();
        // 返回的集合
        List<UserLikeVo> voList = new ArrayList<>();

        //4. 遍历查询的数据，封装返回结果UserLikeVo
        if (list != null && list.size() >0) {
            for (Map<String, Object> map : list) {
                // 获取用户id
                Long uid = (Long) map.get("uid");
                // 获取缘分值
                Long score = (Long) map.get("score");

                // 创建vo对象
                UserLikeVo vo = new UserLikeVo();
                // 根据用户id查询
                UserInfo userInfo = userInfoApi.findById(uid);
                if (userInfo != null) {
                    BeanUtils.copyProperties(userInfo,vo);
                }
                // 设置缘分值
                vo.setMatchRate(score.intValue());
                // vo添加到集合
                voList.add(vo);
            }
        }
        //5. vo集合设置到分页对象中
        pageResult.setItems(voList);
        return ResponseEntity.ok(pageResult);
    }

    @Reference
    private FriendApi friendApi;

    /**
     * 接口名称：粉丝 - 喜欢
     * 接口路径：POST/users/fans/:uid
     */
    public ResponseEntity<Object> fansLike(Long likeUserId) {
        //1. 删除粉丝中的喜欢数据
        userLikeApi.delete(likeUserId,UserHolder.getUserId());
        //2. 记录双向好友关系
        friendApi.save(UserHolder.getUserId(),likeUserId);
        //3. 记录好友关系到环信
        huanXinTemplate.contactUsers(UserHolder.getUserId(),likeUserId);
        return ResponseEntity.ok(null);
    }
}
